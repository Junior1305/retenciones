﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Security
{
    public class filterSecurity : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            object cliente = filterContext.HttpContext.Session["Usuario"];
            if (cliente == null)
            {
                filterContext.Result = new RedirectResult("../Security/LogIn");
            }
        }

    }
}