﻿var contenido = "";
var idPadre = 0;
window.onload = function () {
    $("#loaderOverlay").show();
    sendServer({
        method: "GET",
        url: "../Main/listarMenu"
    }).then(function (result) {
        mostrarMenu(result.responseText)
    });
}

function ObtenerFecha(IdFecha) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    //var hoy = dd + '/' + mm + '/' + yyyy;
    var hoy = yyyy + '-' + mm + '-' + dd;
    document.getElementById(IdFecha).value = hoy;
}

function ObtenerAnhio(Id_select) {
    var d = new Date();
    var n = d.getFullYear();
    var valores = '<option value="0">--Seleccione--</option>';
    for (var i = n; i >= 2000; i--) {
        valores += '<option value="' + i + '">' + i + '</option>';
    }
    document.getElementById(Id_select).innerHTML = valores;
}

function mostrarMenu(rpta) {
    if (rpta != "") {
        crearMenu(rpta.split("*"));
    }
}

function crearMenu(lista) {

    var nfilas = lista.length;
    var campos;

    contenido += '<ul>';
    contenido += "<li class='left_nav_active theme_border' ><a class='inicio' href='javascript:window.location.reload(true)'><i class='fa fa-home'></i><b>Inicio</b></a></li>";
    var contador = 0;
    for (var i = 0; i < nfilas; i++) {
        contador++;
        campos = lista[i].split("~");
        ncolumnas = campos.length;
        
        if (campos[0] == campos[1]) {
            contenido += '<li>'; 
            contenido += "<a href=\"javascript:void(0);\"> <i class='fa " + campos[5] + "'><\/i>";
            contenido += campos[2];
            contenido += "<span class=\"plus\"><i class=\"fa fa-plus\"><\/i><\/span>";
            contenido += "<\/a>";
            idPadre = campos[1]
             contenido += '<ul>'; 

            crearSubMenu(lista, idPadre);
            contenido += "<\/ul>";
            contenido += "<\/li>";
        }
    }
    contenido += "<\/ul>";

    var div = document.getElementsByClassName("left_nav_slidebar");

    div.item(0).innerHTML = contenido;
    $('.left_nav_slidebar ul li').on('click', function (e) {
        e.stopPropagation();
        if ($(this).has('ul').length) {
            var current_class = $('ul', this).attr("class");
            if (current_class != 'opened') {
                $(this).find('ul:visible').slideToggle("normal");
                $(this).find('ul:visible').removeClass("opened");
            }
            if (current_class == 'opened') {
                $('ul', this).removeClass("opened");
                $('ul', this).parent().removeClass('left_nav_active theme_border');
            }
            else {
                $('.left_nav_slidebar ul li').find('ul:visible').parent().removeClass('left_nav_active theme_border');
                $('.left_nav_slidebar ul li').find('ul:visible').slideToggle("normal");
                $('.left_nav_slidebar ul li').find('ul:visible').removeClass("opened");
                $('ul', this).addClass("opened");
                $('ul', this).parent().addClass('left_nav_active theme_border');
            }
            $('ul', this).slideToggle("normal");
        }
    });
}

function crearSubMenu(lista, idPadre) {

    var nFilas = lista.length;
    var campos;
    var contador = 0;
    var hijo = 0;

    for (var i = 0; i < nFilas; i++) {
        campos = lista[i].split("~");
        hijo = campos[0];
        if (campos[0] != campos[1]) {
            if (idPadre == campos[1] && campos[0] != campos[1]) {

                contenido += "<li class='sub_menu_hover'><a style=\"cursor: pointer\" onclick= \"nuevo('";
                contenido += campos[3];
                contenido += "','";
                contenido += campos[2];
                contenido += "','";
                contenido += "1";
                contenido += "','";
                contenido += campos[4];
                contenido += "');\"";
                contenido += "><i class='fa "+ campos[5]+"'><\/i>";
                contenido += "<b>";
                contenido += campos[2];
                contenido += "<\/b>";
                contenido += "<\/a>";
                contenido += "<\/li>";

                crearSubMenu(lista, campos[0]);
            }
        }
    }
    $("#loaderOverlay").hide();
}

function nuevo(ruta, nombre, idForm, metodo) {
    irToMetodo(idForm, nombre, ruta, metodo);
}


function exportToExcel(tableID) {
    var tab_text = "<table border='2px'><tr>";
    var textRange; var j = 0;
    tab = document.getElementById(tableID); // id of table
    for (j = 0 ; j < tab.rows.length ; j++) {
        tab_text = tab_text;
        tab_text = tab_text + tab.rows[j].innerHTML.toUpperCase() + "</tr>";
        //tab_text=tab_text+"</tr>";
    }
    tab_text = tab_text + "</table>";
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); //remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html", "replace");
        txtArea1.document.write('sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa = txtArea1.document.execCommand("SaveAs", true, "sudhir123.txt");
    }
    else {
        sa = window.open('data:application/vnd.ms-excel;charset=utf-8,%EF%BB%BF' + encodeURIComponent(tab_text));
    }
    return (sa);
}