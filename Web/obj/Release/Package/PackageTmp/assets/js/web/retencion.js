﻿function loadMantRetencion(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../Retencion/list_retencion",
                params: {
                    tipoRetencion: "0",
                    ruc: "",
                    fechaEmision: ""
                }
            }).then(function (result) {
                armarlistarRetencion(result.responseText);
                return sendServer({
                    method: "GET",
                    url: "../TipoRetencion/list_tipoRetencion_cbo"
                }).then(function (result) {
                    mostrarListaTipoRetencion(result.responseText);
                    return sendServer({
                        method: "GET",
                        url: "../TipoComprobante/list_tipoComprobante_cbo"
                    }).then(function (result) {
                        mostrarListaTipoComprobante(result.responseText);
                        return sendServer({
                            method: "GET",
                            url: "../Main/ubigeo"
                        }).then(function (result) {
                            mostrarListasUbigeoProveedor(result.responseText);
                            return sendServer({
                                method: "GET",
                                url: "../Proveedor/list_proveedor_cbo"
                            }).then(function (result) {
                                mostrarListaProveedorRetencion(result.responseText);
                                $('[data-toogle="tooltip"]').tooltip();
                                $("#loaderOverlay").hide();
                                $('body').tooltip({
                                    selector: '.btn-table'
                                });
                            });
                        });
                    });
                });
            });

            //Proveedor
            document.getElementById("selectDept").onchange = function () {
                sendServer({
                    method: "GET",
                    url: "../Main/ubigeo"
                }).then(function (result) {
                    mostrarListaProvinciaProveedor(result.responseText);
                    return sendServer({
                        method: "GET",
                        url: "../Main/ubigeo"
                    }).then(function (result) {
                        mostrarListaDistritoProveedor(result.responseText);
                    });
                });
            }

            document.getElementById("selectProv").onchange = function () {
                sendServer({
                    method: "GET",
                    url: "../Main/ubigeo"
                }).then(function (result) {
                    mostrarListaDistritoProveedor(result.responseText);
                });
            }

            document.getElementById("txtRUC_ret").onkeypress = function (e) {
                if (event.which == 13 || event.keyCode == 13) {
                    BuscarSunatRet();
                }
            }


        }
    }
}

function mostrarListaProveedorRetencion(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        autocomplete("txtRuc", lista, "txtRuc");
    }
}

function CorrelativoRetencion() {
    var tipoRetencion = document.getElementById('selectTipoRetencion').value;
    if (tipoRetencion != '0') {
        sendServer({
            method: "GET",
            url: "../TipoRetencion/get_correlativoTipoRetencion",
            params: {
                tipoRetencion: document.getElementById('selectTipoRetencion').value
            }
        }).then(function (result) {
            mostrarCorrelativoTipoRetencion(result.responseText);
        });
    } else {
        document.getElementById('txtNumRetencion').value = '';
    }
}

function mostrarCorrelativoTipoRetencion(rpta) {
    document.getElementById('txtNumRetencion').value = pad(rpta, 8);
}

function monedaRetencion() {
    if (validateMoneda()) {
        var moneda = document.getElementById('selectMoneda').value;
        if (moneda != '2') {
            document.getElementById('txtTipoCambio').value = '1.00';
            document.getElementById('cl-tipo-cambio').style.display = "none";
            addClass(document.getElementById('cl-moneda'), 'col-sm-8');
            removeClass(document.getElementById('cl-moneda'), 'col-sm-4');
        } else {
            var fechaMarcada = document.getElementById('txtFechaEmisionPago').value
            if (fechaMarcada != '') {
                var fechaMatriz = fechaMarcada.split('-');
                sendServer({
                    method: "POST",
                    url: "../TipoCambio/get_TipoCambio_date",
                    params: {
                        año: fechaMatriz[0],
                        mes: fechaMatriz[1],
                        dia: fechaMatriz[2]
                    }
                }).then(function (result) {
                    var tipoCambio = result.responseText;
                    if (tipoCambio == '') {
                        $.alert({
                            closeIcon: true,
                            boxWidth: '25%',
                            useBootstrap: false,
                            title: 'Mensaje',
                            typeAnimated: true,
                            content: '<ul class="jconfirm-error-list"><li>No hay tipo de Cambio(Venta) registrada en la SUNAT</li><li>En caso desee registrar la fecha asignada, dirigase al modulo Tipo de Cambio</li></ul>',
                            buttons: {
                                Aceptar: {
                                    btnClass: 'btn any-other-class'
                                }
                            }
                        });
                        document.getElementById('txtTipoCambio').value = '';
                    } else {
                        document.getElementById('txtTipoCambio').value = result.responseText;
                    }

                });
            } else {
                document.getElementById('txtTipoCambio').value = '';
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Seleccione una fecha.</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
            document.getElementById('cl-tipo-cambio').style.display = "block";
            addClass(document.getElementById('cl-moneda'), 'col-sm-4');
            removeClass(document.getElementById('cl-moneda'), 'col-sm-8');
        }
    }
}

function monedaRetencion2() {
    var moneda = document.getElementById('selectMoneda').value;
    if (moneda == '2') {
        var fechaMarcada = document.getElementById('txtFechaEmisionPago').value
        if (fechaMarcada != '') {
            var fechaMatriz = fechaMarcada.split('-');
            sendServer({
                method: "POST",
                url: "../TipoCambio/get_TipoCambio_date",
                params: {
                    año: fechaMatriz[0],
                    mes: fechaMatriz[1],
                    dia: fechaMatriz[2]
                }
            }).then(function (result) {
                var tipoCambio = result.responseText;
                if (tipoCambio == '') {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>No hay tipo de Cambio(Venta) registrada en la SUNAT</li><li>En caso desee registrar la fecha asignada, dirigase al modulo Tipo de Cambio</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        }
                    });
                    document.getElementById('txtTipoCambio').value = '';
                } else {
                    document.getElementById('txtTipoCambio').value = result.responseText;
                }

            });
        } else {
            document.getElementById('txtTipoCambio').value = '';
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Seleccione una fecha.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        }
    }
}

function validateMoneda() {
    var prob = false;
    $.each($('#listaRetencionDetalle tr'), function (index, value) {
        prob = true;
    });
    var moneda = document.getElementById('selectMoneda').value;
    if (prob) {
        $.confirm({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Estas seguro de cambiar la moneda?</li><li>Las facturas ingresadas tendran que registrarse nuevamente para que obtenga el tipo de cambio asignado</li></ul>',
            buttons: {
                Aceptar: function () {
                    $("#listaRetencionDetalle tr").remove();
                    monedaRetencion();
                    CargaDetalleMonto();
                    return true;
                },
                Cancelar: function () {
                    if (moneda == '1') {
                        $('#selectMoneda option[value="2"]').prop('selected', true);
                    } else {
                        $('#selectMoneda option[value="1"]').prop('selected', true);
                    }
                    return true;
                }
            }
        });
    } else {
        return true;
    }

}

function mostrarCorrelativoTipoRetencion(rpta) {
    document.getElementById('txtNumRetencion').value = pad(rpta, 8);
}

function nuevoRetencion() {
    document.getElementById("modalMessageTitle").innerHTML = "Nueva Retencion";
    $('#modalRetencion').modal(mShow);
    insertInput('modalRetencion');
}

function mostrarListaTipoRetencion(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        var tipoRetencion = [];
        var nRegistros = lista.length;
        var campos;
        for (var i = 0; i < nRegistros; i++) {
            campos = lista[i].split("|");
            if (campos[4] == "1") {
                tipoRetencion.push(campos[0] + "|" + campos[2]);
            }
        }
        crearCombo(tipoRetencion, "selectTipoRetencion", "---");
        crearCombo(tipoRetencion, "selectTipoRetencion_search", "---");
    }
}

function mostrarListaTipoComprobante(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        var tipoComprobante = [];
        var nRegistros = lista.length;
        var campos;
        for (var i = 0; i < nRegistros; i++) {
            campos = lista[i].split("|");
            if (campos[4] == "1") {
                tipoComprobante.push(campos[0] + "|" + campos[2]);
            }
        }
        crearCombo(tipoComprobante, "selectTipoComprobante", "---");
    }
}

function armarlistarRetencion(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaRetencion').firstElementChild != null) {
        $("#tblListaRetencion").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var det = ''; var est = '';
        var option = "<a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"MostrarInfo(this);\" class=\"btn btn-primary btn-table\" title=\"Mirar Info\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a> <a onclick=\"ActualizarEstadoWS(this);\" class=\"btn btn-primary btn-table\" title=\"Actualizar Estado WS\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-refresh\"></i></a>";
        var option2 = "<a onclick=\"EliminarRetencion(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a> <a onclick=\"WebService(this);\" class=\"btn btn-primary btn-table\" title=\"Enviar a Sunat\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-paper-plane-o\"></i></a>";
        var option3 = "<a onclick=\"EliminarRetencion(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a> <a onclick=\"ExportarTxtSunat(this);\" class=\"btn btn-primary btn-table\" title=\"Genera Txt\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-file-text-o\"></i></a>";
        var option4 = "<a onclick=\"EliminarRetencion(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a> <a onclick=\"WebService(this);\" class=\"btn btn-primary btn-table\" title=\"Enviar a Sunat\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-paper-plane-o\"></i></a> <a onclick=\"MostrarInfo(this);\" class=\"btn btn-primary btn-table\" title=\"Mirar Info\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a>";
        var option5 = "<a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"MostrarInfo(this);\" class=\"btn btn-primary btn-table\" title=\"Mirar Info\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a>";
        var option6 = "<a onclick=\"VisualizarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"MostrarInfo(this);\" class=\"btn btn-primary btn-table\" title=\"Mirar Info\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a> <a onclick=\"BajarRetencion(this);\" class=\"btn btn-primary btn-table\" title=\"Anular Retencion WS\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-times\"></i></a>";

        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
                if (j == 2) {
                    det = campo[j].substr(0, 1);
                }
                if (j == 5) {
                    est = campo[j];
                }
            }
           
            if (est == 'Procesado' || est == 'Solicitud de Baja' || est == 'Pendiente') { Valores.push(option); }
            if (est == 'En Proceso') { Valores.push(option2); }
            if (est == 'Registrado') { Valores.push(option3); }
            if (est == 'Error Data' || est == 'Error Tecnico') { Valores.push(option4); }
            if (est == 'Anulado' || est == 'Rechazado') { Valores.push(option5); }
            if (est == 'Aceptado' || est == 'Aceptado con observaciones' || est == 'Error Anulacion') { Valores.push(option6); }
            
            dataSet.push(Valores);
        }
        $('#tblListaRetencion').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Retencion" },
                { title: "Tipo Retencion" },
                { title: "Num Retencion" },
                { title: "Ruc" },
                { title: "Fecha Emision" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaRetencion").innerHTML = "";

        $('#tblListaRetencion').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Retencion" },
                { title: "Tipo Retencion" },
                { title: "Num Retencion" },
                { title: "Ruc" },
                { title: "Fecha Emision" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    }
}

function guardarDetalleRetencion() {
    if (validateInputs('FormularioDetalleRetencion')) {
        if (validateNumeroPago()) {
            var serie = document.getElementById('txtSerie').value;
            var numDocumento = document.getElementById('txtNumero').value;
            if (validateRepeatDetalle(serie, numDocumento)) {
                var datos = '';
                var tipoComprobanteID = document.getElementById('selectTipoComprobante').value;
                var tipoComprobante = $("#selectTipoComprobante>option:selected").html();
                var fechaEmision = document.getElementById('txtFechaEmision').value;
                var monto = document.getElementById('txtMonto').value;
                var numeroPago = document.getElementById('txtNumeroPago').value;
                var pagoParcial = document.getElementById('txtPago').value;
                var moneda = document.getElementById('selectMoneda').value;
                if (moneda != '2') {
                    var montoSinRetencion = (parseFloat(monto) - parseFloat(parseFloat(monto) * 0.03));
                    var retencion1 = parseFloat(parseFloat(monto) * 0.03).toFixed10(3);
                    var retencion = parseFloat(retencion1);
                } else {
                    var tipoCambio = document.getElementById('txtTipoCambio').value;
                    var montoSinRetencion = parseFloat(parseFloat(monto) - (parseFloat(monto) * 0.03)) * tipoCambio;
                    var retencion1 = parseFloat(parseFloat(parseFloat(monto) * 0.03) * tipoCambio).toFixed10(3);
                    var retencion = parseFloat(retencion1);
                }

                if (ValidarMonto700(montoSinRetencion, retencion)) {
                    ActualizarTabla(1);
                    var total = montoSinRetencion + retencion;
                    if (numeroPago != '') {
                        datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a href="#" class=\"btn btn-danger btn-table\" title=\"Pago Parcial: ' + pagoParcial + ' / Numero de Pago: ' + numeroPago + ' \" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a></td>';
                    } else {
                        datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> </td>';
                    }
                    datos += '<td style="display:none;">' + tipoComprobanteID + '</td>';
                    datos += '<td>' + tipoComprobante + '</td>';
                    datos += '<td>' + pad(serie,4) + '</td>';
                    datos += '<td>' + pad(numDocumento,8) + '</td>';
                    datos += '<td>' + fechaEmision + '</td>';
                    datos += '<td>' + montoSinRetencion.toFixed10(2) + '</td>';
                    datos += '<td>' + retencion.toFixed10(2) + '</td>';
                    datos += '<td>' + parseFloat(total).toFixed10(2) + '</td>';
                    datos += '<td style="display:none;">' + numeroPago + '</td>';
                    datos += '<td style="display:none;">' + pagoParcial + '</td></tr>';
                    $('#listaRetencionDetalle').append(datos);
                    CargaDetalleMonto();
                } else {
                    ActualizarTabla(2);
                    var total = montoSinRetencion + retencion;
                    if (numeroPago != '') {
                        datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a href="#" class=\"btn btn-danger btn-table\" title=\"Pago Parcial: ' + pagoParcial + ' / Numero de Pago: ' + numeroPago + ' \" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a></td>';
                    } else {
                        datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> </td>';
                    }
                    datos += '<td style="display:none;">' + tipoComprobanteID + '</td>';
                    datos += '<td>' + tipoComprobante + '</td>';
                    datos += '<td>' + pad(serie, 4) + '</td>';
                    datos += '<td>' + pad(numDocumento, 8) + '</td>';
                    datos += '<td>' + fechaEmision + '</td>';
                    datos += '<td>' + parseFloat(total).toFixed10(2) + '</td>';
                    datos += '<td>0.00</td>';
                    datos += '<td>' + parseFloat(total).toFixed10(2) + '</td>';
                    datos += '<td style="display:none;">' + numeroPago + '</td>';
                    datos += '<td style="display:none;">' + pagoParcial + '</td></tr>';
                    $('#listaRetencionDetalle').append(datos);
                    CargaDetalleMonto();
                }
            } else {
                $.alert({
                    closeIcon: true,
                    boxWidth: '25%',
                    useBootstrap: false,
                    title: 'Mensaje',
                    typeAnimated: true,
                    content: '<ul class="jconfirm-error-list"><li>Serie y numero de Documento repetidos , por favor ingresar uno diferente.</li></ul>',
                    buttons: {
                        Aceptar: {
                            btnClass: 'btn any-other-class'
                        }
                    }
                });
            }
        } 
    }
}

function validateNumeroPago() {
    var numeroPago = document.getElementById('txtNumeroPago').value;
    var pagoParcial = document.getElementById('txtPago').value;

    if (numeroPago.length > 0) {
        if (pagoParcial == '') {
            document.getElementById('txtPago').focus;
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Si ha registrado un numero de Pago , debe ingresar el pago parcial que se realizara.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
            return false;
        }
    }
    if (pagoParcial.length > 0) {
        if (numeroPago == '') {
            document.getElementById('txtNumeroPago').focus;
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Si ha registrado un pago parcial , debe ingresar el numero de pago que se realizara.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
            return false;
        }
    }
    return true;
}

function ValidarMonto700(sin, retencion) {
    var montoRetenido = 0.00;
    var montoSinRetencion = 0.00;
    var montoTotal = 0.00;
    $.each($('#listaRetencionDetalle tr'), function (index, value) {
        montoRetenido = montoRetenido + parseFloat($(this).find('td:nth-child(8)').html());
        montoSinRetencion = montoSinRetencion + parseFloat($(this).find('td:nth-child(7)').html());
    });
    montoTotal = (montoRetenido + montoSinRetencion + sin + retencion);
    if (montoTotal >= 700.00) {
        return true;
    } else {
        return false;
    }
}

function ActualizarTabla(ind) {
    if (ind == 1) {
        $.each($('#listaRetencionDetalle tr'), function (index, value) {
            var val1 = $(this).find('td:nth-child(7)').html();
            var val2 = $(this).find('td:nth-child(8)').html();
            var montoSinRetencion = 0.00; var retencion = 0.00; var sum = 0.00;
            sum = parseFloat(parseFloat(val1) + parseFloat(val2)).toFixed10(2);
            montoSinRetencion = (parseFloat(sum) - (parseFloat(sum) * 0.03));
            retencion = (parseFloat(sum) * 0.03);
            $(this).find('td:nth-child(7)').html(montoSinRetencion.toFixed10(2));
            $(this).find('td:nth-child(8)').html(retencion.toFixed10(2));
            $(this).find('td:nth-child(9)').html(sum);
        });
    } else {
        $.each($('#listaRetencionDetalle tr'), function (index, value) {
            var val1 = $(this).find('td:nth-child(7)').html();
            var val2 = $(this).find('td:nth-child(8)').html();
            var montoSinRetencion = 0.00; var retencion = 0.00; var sum = 0.00;
            sum = parseFloat(parseFloat(val1) + parseFloat(val2)).toFixed10(2);
            $(this).find('td:nth-child(7)').html(sum);
            $(this).find('td:nth-child(8)').html(retencion.toFixed10(2));
            $(this).find('td:nth-child(9)').html(sum);
        });
    }
}

function validateRepeatDetalle(serie, documento) {
    var prob = true;
    $.each($('#listaRetencionDetalle tr'), function (index, value) {
        var serieList = $(this).find('td:nth-child(4)').html();
        var documentoList = $(this).find('td:nth-child(5)').html();
        if (pad(serie,4) == serieList && pad(documento,8) == documentoList) {
            prob = false;
        }
    });
    return prob;
}

function CargaDetalleMonto() {
    var montoRetenido = 0.00;
    var montoSinRetencion = 0.00;
    var montoTotal = 0.00;
    $.each($('#listaRetencionDetalle tr'), function (index, value) {
        montoRetenido = montoRetenido + parseFloat($(this).find('td:nth-child(8)').html());
        montoSinRetencion = montoSinRetencion + parseFloat($(this).find('td:nth-child(7)').html());
    });
    montoTotal = (montoRetenido + montoSinRetencion);
    document.getElementById('txtTotalRetencion').value = montoRetenido.toFixed10(2);
    document.getElementById('txtTotalSinRetencion').value = montoSinRetencion.toFixed10(2);
    document.getElementById('txtTotal').value = parseFloat(montoTotal).toFixed10(2);

}

function EliminarRowDetalle(elemento) {
    var row = elemento.parentNode.parentNode;
    row.remove();
    if (ValidarMonto700(0.00, 0.00)) {
        ActualizarTabla(1);
    } else {
        ActualizarTabla(2);
    }
    CargaDetalleMonto();
}

function AccionRetencion(ind) {
    $("#loaderOverlay").show();
    var rucTxt = document.getElementById('txtRuc');
    sendServer({
        method: "POST",
        url: "../Retencion/search_proveedor",
        params: {
            ruc: document.getElementById('txtRuc').value
        }
    }).then(function (result) {
        $("#loaderOverlay").hide();
        if (result.responseText == '0') {
            addClass(rucTxt, 'red-validate');
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Proveedor no registrado,  Ingreselo al modulo del proveedor.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
            });
        } else {
            removeClass(rucTxt, 'red-validate');
            armarSearchProveedorRetencion(ind);
        }
    }).catch(function (error) {
        console.log('Ocurrió un error: ', error);
    });
}

function armarSearchProveedorRetencion(ind) {
    if (ind == 'I') {
        if (validateInputsRetencion()) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Retencion/search_retencion",
                params: {
                    tipo: document.getElementById('selectTipoRetencion').value,
                    num: document.getElementById('txtNumRetencion').value
                }
            }).then(function (result) {
                armarSearchRetencion(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputsRetencion()) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Retencion/search_retencion_id",
                params: {
                    id: document.getElementById('retencionIDHid').value,
                    tipo: document.getElementById('selectTipoRetencion').value,
                    num: document.getElementById('txtNumRetencion').value
                }
            }).then(function (result) {
                armarSearchRetencion(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}



function validateInputsRetencion() {
    var prob = ''; var valr = true;
    var tipoRetencion = document.getElementById('selectTipoRetencion');
    var numRetencion = document.getElementById('txtNumRetencion');
    var ruc = document.getElementById('txtRuc');
    var fechaEmision = document.getElementById('txtFechaEmisionPago');
    var moneda = document.getElementById('selectMoneda');
    var tableDetalle = document.getElementById('tblListaRetencionDetalle');
    if (tipoRetencion.value == '0') { addClass(tipoRetencion, 'red-validate'); prob += '<li>Seleccione el tipo de Retencion</li>'; } else { removeClass(tipoRetencion, 'red-validate'); }
    if (numRetencion.value == '') { addClass(numRetencion, 'red-validate'); prob += '<li>Ingrese el numero de Retencion</li>'; } else { removeClass(numRetencion, 'red-validate'); }
    if (ruc.value == '') { addClass(ruc, 'red-validate'); prob += '<li>Ingrese el RUC</li>'; } else { removeClass(ruc, 'red-validate'); }
    if (fechaEmision.value == '') { addClass(fechaEmision, 'red-validate'); prob += '<li>Ingrese la fecha de Emision</li>'; } else { removeClass(fechaEmision, 'red-validate'); }
    if (moneda.value == '0') { addClass(moneda, 'red-validate'); prob += '<li>Seleccione la moneda</li>'; } else { removeClass(moneda, 'red-validate'); }
    if ($('#listaRetencionDetalle tr').length == 0) { addClass(tableDetalle, 'red-validate'); prob += '<li>Ingrese una factura para la retencion</li>'; } else { removeClass(tableDetalle, 'red-validate'); }

    if (prob != '') {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list">' + prob + '</ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
        valr = false;
    }

    return valr;
}

function armarSearchRetencion(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../Retencion/new_retencion",
                params: {
                    tipo: document.getElementById('selectTipoRetencion').value,
                    numRetencion: document.getElementById('txtNumRetencion').value,
                    ruc: document.getElementById('txtRuc').value,
                    fechaEmision: document.getElementById('txtFechaEmisionPago').value,
                    moneda: document.getElementById('selectMoneda').value,
                    tipoCambio: document.getElementById('txtTipoCambio').value,
                    detalle: listaDetalleRetencion(),
                    montoSinRetencion: document.getElementById('txtTotalRetencion').value,
                    retencion: document.getElementById('txtTotalSinRetencion').value,
                    montoTotal: document.getElementById('txtTotal').value,
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalRetencion').modal(mHide);
                armarCondRetencion(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../Retencion/update_retencion",
                params: {
                    id: document.getElementById('retencionIDHid').value,
                    tipo: document.getElementById('selectTipoRetencion').value,
                    numRetencion: document.getElementById('txtNumRetencion').value,
                    ruc: document.getElementById('txtRuc').value,
                    fechaEmision: document.getElementById('txtFechaEmisionPago').value,
                    moneda: document.getElementById('selectMoneda').value,
                    tipoCambio: document.getElementById('txtTipoCambio').value,
                    detalle: listaDetalleRetencion(),
                    montoSinRetencion: document.getElementById('txtTotalRetencion').value,
                    retencion: document.getElementById('txtTotalSinRetencion').value,
                    montoTotal: document.getElementById('txtTotal').value,
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalRetencion').modal(mHide);
                armarCondRetencion(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Retencion existente, Verifique en la lista de retenciones.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function listaDetalleRetencion() {
    var prob = '';
    $.each($('#listaRetencionDetalle tr'), function (index, value) {
        prob += $(this).find('td:nth-child(2)').html() + '¬' + $(this).find('td:nth-child(4)').html() + '¬' + $(this).find('td:nth-child(5)').html() + '¬' + $(this).find('td:nth-child(6)').html() + '¬' + $(this).find('td:nth-child(7)').html() + '¬' + $(this).find('td:nth-child(8)').html() + '¬' + $(this).find('td:nth-child(10)').html() + '¬' + $(this).find('td:nth-child(11)').html() + '^';
    });
    prob = prob.substr(0, prob.length - 1);

    return prob;
}

function armarCondRetencion(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
        }
    });
}

function ClearRetencion() {
    clearInput('FormularioRetencion');
    filtrarListaRetencion();
}

function filtrarListaRetencion() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../Retencion/list_retencion",
        params: {
            tipoRetencion: document.getElementById('selectTipoRetencion_search').value,
            ruc: document.getElementById('txtRuc_search').value,
            fechaEmision: document.getElementById('txtFechaEmision_search').value
        }
    }).then(function (result) {
        armarlistarRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarRetencion(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar la retencion?</li><li>En caso existan facturas registradas, se eliminaran junta a la retencion</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../Retencion/delete_retencion",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../Retencion/list_retencion",
                                params: {
                                    tipoRetencion: document.getElementById('selectTipoRetencion_search').value,
                                    ruc: document.getElementById('txtRuc_search').value,
                                    fechaEmision: document.getElementById('txtFechaEmision_search').value
                                }
                            }).then(function (result) {
                                armarlistarRetencion(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarRetencion(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Retencion";
    $('#modalRetencion').modal(mShow);
    viewInput('modalRetencion');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/show_retencion",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarRetencion(rpta) {
    var data = rpta.split("|");
    document.getElementById('retencionIDHid').value = data[0];
    document.getElementById('selectTipoRetencion').value = data[1];
    document.getElementById('txtNumRetencion').value = data[2];
    document.getElementById('txtRuc').value = data[3];
    document.getElementById('txtFechaEmisionPago').value = data[4];
    document.getElementById('selectMoneda').value = data[5];
    if (data[5] == '2') {
        document.getElementById('cl-tipo-cambio').style.display = "block";
        addClass(document.getElementById('cl-moneda'), 'col-sm-4');
        removeClass(document.getElementById('cl-moneda'), 'col-sm-8');
        document.getElementById('txtTipoCambio').value = data[6];
    } else {
        document.getElementById('cl-tipo-cambio').style.display = "none";
        addClass(document.getElementById('cl-moneda'), 'col-sm-8');
        removeClass(document.getElementById('cl-moneda'), 'col-sm-4');
        document.getElementById('txtTipoCambio').value = data[6];
    }
    document.getElementById('txtTotalRetencion').value = data[7];
    document.getElementById('txtTotalSinRetencion').value = data[8];
    document.getElementById('txtTotal').value = data[9];
    CargarTablaDetalleRetencion(data[10]);
}

function CargarTablaDetalleRetencion(rpta) {
    var data = rpta.split("^");
    for (var i = 0; i < data.length; i++) {
        var datos = '';
        var data2 = data[i].split("¬");
        if (data2[8] != '') {   
            datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a href="#" class=\"btn btn-danger btn-table\" title=\"Pago Parcial: ' + data2[9] + ' / Numero de Pago: ' + data2[8] + ' \" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-info-circle\"></i></a></td>';
        } else {
            datos += '<tr><td><a onclick=\"EliminarRowDetalle(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> </td>';
        }
        datos += '<td style="display:none;">' + data2[0] + '</td>';
        datos += '<td>' + data2[1] + '</td>';
        datos += '<td>' + data2[2] + '</td>';
        datos += '<td>' + data2[3] + '</td>';
        datos += '<td>' + data2[4] + '</td>';
        datos += '<td>' + data2[5] + '</td>';
        datos += '<td>' + data2[6] + '</td>';
        datos += '<td>' + data2[7] + '</td>';
        datos += '<td style="display:none;">' + data2[8] + '</td>';
        datos += '<td style="display:none;">' + data2[9] + '</td></tr>';
        $('#listaRetencionDetalle').append(datos);
    }
}

function EditarRetencion(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Retencion";
    $('#modalRetencion').modal(mShow);
    editInput('modalRetencion');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/show_retencion",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function WebService(elemento) {
    $("#loaderOverlay").show();
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/web_service",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarResultadoWebService(result.responseText);
        $("#loaderOverlay").hide();
    });

}

function armarResultadoWebService(rpta) {
    if (rpta == '1') {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Envio de Retencion satisfactoria.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
            }
        });
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Envio de Retencion con errores. Revisar informacion detallada</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
            }
        });
    }
}

function MostrarInfo(elemento) {
    $("#loaderOverlay").show();
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/web_service_info",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarResultadoMostrarInfo(result.responseText);
    });
}

function armarResultadoMostrarInfo(rpta) {
    var matriz = rpta.split('¥');
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estado: ' + matriz[0] + '</li><li>Detalle: <br>' + matriz[2].replaceAll('|','<br>').replaceAll(';','<br>').replaceAll('Error','<strong>Error</strong>').replaceAll('*','<hr><br>') + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        }
    });
}

function ExportarTxtSunat(elemento) {
    $("#loaderOverlay").show();
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/web_service_txt",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarResultadoTxtRetencion(result.responseText);
        $("#loaderOverlay").hide();
    });
    
}

function armarResultadoTxtRetencion(rpta) {
    var matriz = rpta.split('¥');
    var blob = new Blob([matriz[0].replaceAll('*','\n')], { type: "text/plain;charset=utf-8" });
    saveAs(blob, matriz[1]+".txt");
}

function BajarRetencion(elemento) {
    $.confirm({
        title: 'Mensaje',
        content: '' +
        '<form action="" class="formName" style="height:80px">' +
        '<div class="form-group"><div class="col-sm-12">' +
        '<label>Ingresar Motivo de Baja</label>' +
        '<input type="text" placeholder="Motivo de Baja" class="name form-control" required max-length="100"/>' +
        '</div></div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Enviar',
                btnClass: 'btn-blue',
                action: function () {
                    var name = this.$content.find('.name').val();
                    if (!name) {
                        $.alert('Ingrese un motivo de baja');
                        return false;
                    }
                    $("#loaderOverlay").show();
                    var row = elemento.parentNode.parentNode;
                    sendServer({
                        method: "POST",
                        url: "../Retencion/web_service_baja",
                        params: {
                            id: row.children[0].innerHTML,
                            motivoBaja: name
                        }
                    }).then(function (result) {
                        armarResultadoRetencionBaja(result.responseText);
                        $("#loaderOverlay").hide();
                    });
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });


   
}

function armarResultadoRetencionBaja(rpta) {
    if (rpta == '1') {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Baja de Retencion satisfactoria.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
            }
        });
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Baja de Retencion con errores. Revisar informacion detallada</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
            }
        });
    }
}

function ActualizarEstadoWS(elemento) {
    $("#loaderOverlay").show();
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Retencion/web_service_consulta",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarResultadoMostrarConsulta(result.responseText);
    });
}

function armarResultadoMostrarConsulta(rpta) {
    if (rpta == '1') {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Consulta Realizada Satisfactoriamente.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../Retencion/mant_retencion', 'Retencion', '1', 'loadMantRetencion');
            }
        });
    }
}
//Proveedor ------------------------------------------------------

function nuevoProveedorRetencion() {
    document.getElementById("modalMessageTitle2").innerHTML = "Nuevo Proveedor";
    $('#modalProveedor').modal(mShow);
    insertInput('modalProveedor');
}

function AccionProveedorRet(ind) {
    if (ind == 'I') {
        if (validateInputs('modalProveedor')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Proveedor/search_proveedor",
                params: {
                    ruc: document.getElementById('txtRUC_ret').value,
                }
            }).then(function (result) {
                armarSearchProveedorRet(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchProveedorRet(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../Proveedor/new_proveedor",
                params: {
                    ruc: document.getElementById('txtRUC_ret').value,
                    razonSocial: document.getElementById('txtRazonSocial').value.toUpperCase(),
                    direccion: document.getElementById('txtDireccion').value.toUpperCase(),
                    email: document.getElementById('txtEmail').value.toUpperCase(),
                    telefono: document.getElementById('txtTelefono').value,
                    estado_s: document.getElementById('txtEstado').value.toUpperCase(),
                    condicion: document.getElementById('txtCondicion').value.toUpperCase(),
                    departamento: document.getElementById('selectDept').value,
                    provincia: document.getElementById('selectProv').value,
                    distrito: document.getElementById('selectDist').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalProveedor').modal(mHide);
                armarCondProveedorRet(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Proveedor existente, Verifique en la lista de proveedores.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondProveedorRet(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            $('#modalProveedor').modal(mHide);
            sendServer({
                method: "GET",
                url: "../Proveedor/list_proveedor_cbo"
            }).then(function (result) {
                mostrarListaProveedorRetencion(result.responseText);
            });
        }
    });
}

function BuscarSunatRet() {
    $("#loaderOverlay").show();
    var tipo = document.getElementById('txtRUC_ret').value;
    sendServer({
        method: "GET",
        url: "../Main/getSunat",
        params: {
            ruc: document.getElementById('txtRUC_ret').value,
            tipo: tipo.substr(0, 2)
        }
    }).then(function (result) {
        var data = result.responseText;
        var matriz = data.split('|');
        if (data.length > 0) {
            if (tipo.substr(0, 2) != '10') {
                document.getElementById('txtRazonSocial').value = matriz[0].substr(39, matriz[0].length);
                document.getElementById('txtEstado').value = matriz[1];
                document.getElementById('txtCondicion').value = matriz[2];
                document.getElementById('txtDireccion').value = matriz[3];
                var matriz = matriz[3].split('-');
                var Dept = matriz[0].replace('PROV. CONST. DEL ', '').trim();
                var Prov = matriz[1].trim();
                var Dist = matriz[2].trim();
                $("#selectDept option").each(function (index) {
                    if (Dept.indexOf($(this).text()) != -1) {
                        $("#selectDept").find('option:contains("' + $(this).text() + '")').prop('selected', true);
                    }
                });

                ListarProvinciaViewProveedorByText(Prov);
                ListarDistritoViewProveedorByText(Dist);
            } else {
                document.getElementById('txtRazonSocial').value = matriz[0];
                document.getElementById('txtEstado').value = matriz[1];
                document.getElementById('txtCondicion').value = matriz[2];
                document.getElementById('txtDireccion').value = matriz[3];
                $("#selectDept").find('option:contains("---")').prop('selected', true);
                $("#selectProv").find('option:contains("---")').prop('selected', true);
                $("#selectDist").find('option:contains("---")').prop('selected', true);
            }

        } else {
            $("#loaderOverlay").hide();
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Conexion Perdida, Intente de Nuevo.</li><li>En caso de que persista, espere unos 10 min para realizar la consulta a la sunat.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }

            });
        }
        $("#loaderOverlay").hide();
    });
}

//Edn Proveedor -----------------------------------------------------