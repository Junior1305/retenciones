﻿function loadMantTipoCambio(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../TipoCambio/list_tipoCambio",
                params: {
                    fecha: "1999-99-99"
                }
            }).then(function (result) {
                armarlistarTipoCambio(result.responseText);
                seccionSunat();
                cargarMes('mes_charge');
                cargarAño('año_charge');
                $('[data-toogle="tooltip"]').tooltip();
                $("#loaderOverlay").hide();
            });
        }
    }
}

function ModalSunatTipoCambio() {
    $('#modalTipoCambioSunat').modal(mShow);
}

function ModalSunatListaTipoCambio() {
    $('#modalNewTipoCambio').modal(mShow);
}

function CargarListaSunat() {
    $("#loaderOverlay").show();
    var añoCargado = document.getElementById('año_charge').value;
    var mesCargado = document.getElementById('mes_charge').value;
    sendServer({
        method: "POST",
        url: "../Main/cargarListaSunat",
        params: {
            año: añoCargado,
            mes: mesCargado
        }
    }).then(function (result) {
        $("#loaderOverlay").hide();
        $('#modalNewTipoCambio').modal(mHide);
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>'+ result.responseText + '</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            },
            onClose: function () {
                nuevo('../TipoCambio/mant_tipoCambio', 'TipoCambio', '1', 'loadMantTipoCambio');
            }
        });
    });
}

function nuevoTipoCambio() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Tipo de Cambio";
    $('#modalTipoCambio').modal(mShow);
    insertInput('modalTipoCambio');
}

function armarlistarTipoCambio(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaTipoCambio').firstElementChild != null) {
        $("#tblListaTipoCambio").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarTipoCambio(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarTipoCambio(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarTipoCambio(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaTipoCambio').DataTable({
            data: dataSet,
            "order": [[ 1, "desc" ]],
            columns: [
                { title: "Id Tipo Cambio" },
                { title: "Fecha" },
                { title: "Tipo de Venta" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0,3],
                className: "hide_column"
            },
            {
                "targets": [0,3],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaTipoCambio").innerHTML = "";

        $('#tblListaTipoCambio').DataTable({
            "order": [[1, "desc"]],
            data: dataSet,
            columns: [
                { title: "Id Tipo Cambio" },
                { title: "Fecha" },
                { title: "Tipo de Venta" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0,3],
                className: "hide_column"
            },
            {
                "targets": [0,3],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function AccionTipoCambio(ind) {
    if (ind == 'I') {
        if (validateInputs('modalTipoCambio')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoCambio/search_tipoCambio",
                params: {
                    fecha: document.getElementById('txtFecha').value,
                }
            }).then(function (result) {
                armarSearchTipoCambio(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalTipoCambio')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoCambio/search_tipoCambio_id",
                params: {
                    id: document.getElementById('tipoCambioIDHid').value,
                    fecha: document.getElementById('txtFecha').value
                }
            }).then(function (result) {
                armarSearchTipoCambio(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchTipoCambio(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../TipoCambio/new_tipoCambio",
                params: {
                    fecha: document.getElementById('txtFecha').value,
                    venta: document.getElementById('txtVenta').value,
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoCambio').modal(mHide);
                armarCondTipoCambio(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../TipoCambio/update_tipoCambio",
                params: {
                    id: document.getElementById('tipoCambioIDHid').value,
                    fecha: document.getElementById('txtFecha').value,
                    venta: document.getElementById('txtVenta').value,
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoCambio').modal(mHide);
                armarCondTipoCambio(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Fecha existente, Verifique en la lista de fechas creadas.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondTipoCambio(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../TipoCambio/mant_tipoCambio', 'TipoCambio', '1', 'loadMantTipoCambio');
        }
    });
}

function ClearTipoCambio() {
    clearInput('FormularioTipoCambio');
    filtrarListaTipoCambio();
}

function filtrarListaTipoCambio() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../TipoCambio/list_tipoCambio",
        params: {
            fecha: document.getElementById('txtFecha_search').value
        }
    }).then(function (result) {
        armarlistarTipoCambio(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarTipoCambio(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar el tipo de Cambio?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../TipoCambio/delete_tipoCambio",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            var fechaSeleccionado = document.getElementById('txtFecha_search').value;
                            if (fechaSeleccionado == '') { fechaSeleccionado = '1999-99-99'; }
                            return sendServer({
                                method: "POST",
                                url: "../TipoCambio/list_tipoCambio",
                                params: {
                                    fecha: fechaSeleccionado
                                }
                            }).then(function (result) {
                                armarlistarTipoCambio(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarTipoCambio(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Tipo de Cambio";
    $('#modalTipoCambio').modal(mShow);
    viewInput('modalTipoCambio');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoCambio/show_tipoCambio",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoCambio(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarTipoCambio(rpta) {
    var data = rpta.split("|");
    document.getElementById('tipoCambioIDHid').value = data[0];
    document.getElementById('txtFecha').value = data[1];
    document.getElementById('txtVenta').value = data[2];
}

function EditarTipoCambio(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Tipo de Cambio";
    $('#modalTipoCambio').modal(mShow);
    editInput('modalTipoCambio');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoCambio/show_tipoCambio",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoCambio(result.responseText);
        $("#loaderOverlay").hide();
    });
}


function seccionSunat() {
    sendServer({
        method: "POST",
        url: "../Main/obtenerTipoCambioHtml"
    }).then(function (result) {
        var contenido = "";
        var parse = new DOMParser()
        var el = parse.parseFromString(result.responseText, "text/html");

        var tituloMes = el.firstChild.getElementsByTagName("h3");
        var htmlTable = el.firstChild.getElementsByTagName("table");
        if (htmlTable.length > 0) {
            for (var j = 0; j < tituloMes.length; j++) {
                tituloMes[j].setAttribute("class", "center");
                contenido += tituloMes[j].outerHTML;
                break;
            }
            for (var i = 0; i <= 3; i++) {
                htmlTable[i].removeAttribute("class");
                htmlTable[i].setAttribute("class", "table table-bordered");
                contenido += htmlTable[i].outerHTML;
                document.getElementById("sunat").innerHTML = contenido;
            }
        } else {
            document.getElementById("sunat").innerHTML = result.responseText;
        }
        document.getElementsByClassName("button")[0].setAttribute("onclick", "buscarTcxMesAnio();");
        document.getElementsByClassName("button")[0].setAttribute("class", "btn btn-success");
        document.getElementById("sunat").style.display = "block";
    });
}

function buscarTcxMesAnio() {
    sendServer({
        method: "POST",
        url: "../Main/obtenerTipoCambioHtml",
        params: {
            mes: document.getElementsByName("mes")[0].value,
            anio: document.getElementsByName("anho")[0].value
        }
    }).then(function (result) {
        var rpta = result.responseText;
        var contenido = "";
        var parse = new DOMParser()
        var el = parse.parseFromString(rpta, "text/html");

        var tituloMes = el.firstChild.getElementsByTagName("h3");
        var htmlTable = el.firstChild.getElementsByTagName("table");
        if (htmlTable.length > 0) {
            for (var j = 0; j < tituloMes.length; j++) {
                tituloMes[j].setAttribute("class", "center");
                contenido += tituloMes[j].outerHTML;
                break;
            }
            for (var i = 0; i <= 3; i++) {
                htmlTable[i].removeAttribute("class");
                htmlTable[i].setAttribute("class", "table table-bordered");
                contenido += htmlTable[i].outerHTML;
                document.getElementById("sunat").innerHTML = contenido;
            }
        } else {
            document.getElementById("sunat").innerHTML = rpta;
        }
        document.getElementsByClassName("button")[0].setAttribute("onclick", "buscarTcxMesAnio();");
        document.getElementsByClassName("button")[0].setAttribute("class", "btn btn-success");
        document.getElementById("sunat").style.display = "block";

    });
}