﻿function loadMantTipoComprobante(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../TipoComprobante/list_tipoComprobante",
                params: {
                    codigo: "",
                    estado: 0
                }
            }).then(function (result) {
                armarlistarTipoComprobante(result.responseText);
                $('[data-toogle="tooltip"]').tooltip();
                $("#loaderOverlay").hide();
            });
        }
    }
}

function nuevoTipoComprobante() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Tipo de Comprobante";
    $('#modalTipoComprobante').modal(mShow);
    insertInput('modalTipoComprobante');
}

function armarlistarTipoComprobante(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaTipoComprobante').firstElementChild != null) {
        $("#tblListaTipoComprobante").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarTipoComprobante(this);\" class=\"btn btn-danger btn-table\" title=\"Desactivar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarTipoComprobante(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarTipoComprobante(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaTipoComprobante').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Tipo Comprobante" },
                { title: "Codigo" },
                { title: "Nomenclatura" },
                { title: "Nombre" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0],
                className: "hide_column"
            },
            {
                "targets": [0],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaTipoComprobante").innerHTML = "";

        $('#tblListaTipoComprobante').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Tipo Comprobante" },
                { title: "Codigo" },
                { title: "Nomenclatura" },
                { title: "Nombre" },
                { title: "Estado" },
                { title: "Opciones" }
            ],
            columnDefs: [
            {
                "targets": [0],
                className: "hide_column"
            },
            {
                "targets": [0],
                "searchable": false
            }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function AccionTipoComprobante(ind) {
    if (ind == 'I') {
        if (validateInputs('modalTipoComprobante')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoComprobante/search_tipoComprobante",
                params: {
                    codigo: document.getElementById('txtcodigoComprobante').value,
                }
            }).then(function (result) {
                armarSearchTipoComprobante(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalTipoComprobante')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../TipoComprobante/search_tipoComprobante_id",
                params: {
                    id: document.getElementById('tipoComprobanteIDHid').value,
                    codigo: document.getElementById('txtcodigoComprobante').value
                }
            }).then(function (result) {
                armarSearchTipoComprobante(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchTipoComprobante(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../TipoComprobante/new_tipoComprobante",
                params: {
                    codigo: document.getElementById('txtcodigoComprobante').value.toUpperCase(),
                    nomenclatura: document.getElementById('txtNomenclatura').value.toUpperCase(),
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoComprobante').modal(mHide);
                armarCondTipoComprobante(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../TipoComprobante/update_tipoComprobante",
                params: {
                    id: document.getElementById('tipoComprobanteIDHid').value,
                    codigo: document.getElementById('txtcodigoComprobante').value.toUpperCase(),
                    nomenclatura: document.getElementById('txtNomenclatura').value.toUpperCase(),
                    nombre: document.getElementById('txtNombre').value.toUpperCase(),
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalTipoComprobante').modal(mHide);
                armarCondTipoComprobante(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Tipo Comprobante existente, Verifique en la lista de comprobantes.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondTipoComprobante(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../TipoComprobante/mant_tipoComprobante', 'Tipo Comprobante', '1', 'loadMantTipoComprobante');
        }
    });
}

function ClearTipoComprobante() {
    clearInput('FormularioTipoComprobante');
    filtrarListaTipoComprobante();
}

function filtrarListaTipoComprobante() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../TipoComprobante/list_tipoComprobante",
        params: {
            codigo: document.getElementById('txtCodigoComprobante_search').value,
            estado: document.getElementById('selectEstado_search').value
        }
    }).then(function (result) {
        armarlistarTipoComprobante(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarTipoComprobante(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Desactivar el tipo de comprobante?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../TipoComprobante/delete_tipoComprobante",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../TipoComprobante/list_tipoComprobante",
                                params: {
                                    codigo: document.getElementById('txtCodigoComprobante_search').value,
                                    estado: document.getElementById('selectEstado_search').value
                                }
                            }).then(function (result) {
                                armarlistarTipoComprobante(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarTipoComprobante(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Tipo de Comprobante";
    $('#modalTipoComprobante').modal(mShow);
    viewInput('modalTipoComprobante');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoComprobante/show_tipoComprobante",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoComprobante(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarTipoComprobante(rpta) {
    var data = rpta.split("|");
    document.getElementById('tipoComprobanteIDHid').value = data[0];
    document.getElementById('txtcodigoComprobante').value = data[1];
    document.getElementById('txtNomenclatura').value = data[2];
    document.getElementById('txtNombre').value = data[3];
    putRadioValue('estado', data[4]);
}

function EditarTipoComprobante(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Tipo de Comprobante";
    $('#modalTipoComprobante').modal(mShow);
    editInput('modalTipoComprobante');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../TipoComprobante/show_tipoComprobante",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarTipoComprobante(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarTipoComprobante(rpta) {
    var data = rpta.split("|");
    document.getElementById('tipoComprobanteIDHid').value = data[0];
    document.getElementById('txtcodigoComprobante').value = data[1];
    document.getElementById('txtNomenclatura').value = data[2];
    document.getElementById('txtNombre').value = data[3];
    putRadioValue('estado', data[4]);
}