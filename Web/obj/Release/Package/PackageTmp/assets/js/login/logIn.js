﻿var sendServer = function (opts) {
    var urlRaiz = document.getElementById("hdfRaiz").value;
    var urlBase = "http://" + window.location.host + urlRaiz;
    window.localStorage.setItem("urlBase", urlBase)

    var xhr = new XMLHttpRequest;
    return new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) return;
            if (xhr.status >= 200 && xhr.readyState == 4) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        }
        var params = opts.params;
        if (params && typeof params === 'object') {
            opts.method = "POST";
            params = Object.keys(params).map(function (key) {
                //return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
                return encodeURIComponent(params[key]);
            }).join('|');
        }
        xhr.open(opts.method || 'GET', urlBase + opts.url, true);
        xhr.send(params)
    });
}

var sendServerLog = function (opts) {
    var urlRaiz = document.getElementById("hdfRaiz").value;
    var urlBase = "http://" + window.location.host + urlRaiz;
    window.localStorage.setItem("urlBase", urlBase)


    var xhr = new XMLHttpRequest;
    return new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) return;
            if (xhr.status >= 200 && xhr.readyState == 4) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        }
        var params = opts.params; var log = opts.params;
        if (params && typeof params === 'object') {
            opts.method = "POST";
            params = Object.keys(params).map(function (key) {
                return encodeURIComponent(params[key]);
            }).join('|');

            log = Object.keys(log).map(function (key2) {
                return encodeURIComponent(key2 + ': ' + log[key2]);
            }).join(',');
        }
        xhr.open(opts.method || 'GET', opts.url, true);
        var sendr = params + encodeURIComponent('¥') + log;
        xhr.send(sendr)
    });
}

var btn = document.getElementById("btnAceptar").onclick = function () {
    $("#loaderOverlay").show();
    var user = document.getElementById('txtUsuario').value;
    var pass = document.getElementById('txtClave').value;
    if (ValidateLogin(user, pass)) {
        sendServerLog({
            method: "POST",
            url: "Security/validarLogin",
            params: {
                user: document.getElementById('txtUsuario').value,
                pass: document.getElementById('txtClave').value
            }
        }).then(function (result) {
            mostrarMsgValidacion_Login(result.responseText);
            $("#loaderOverlay").hide();
        });
    }

}

var btn2 = document.getElementById('txtClave').onkeypress = function (e) {
    if (e.keyCode == 13) {
        $("#loaderOverlay").show();
        var user = document.getElementById('txtUsuario').value;
        var pass = document.getElementById('txtClave').value;
        if (ValidateLogin(user, pass)) {
            sendServerLog({
                method: "POST",
                url: "Security/validarLogin",
                params: {
                    user: document.getElementById('txtUsuario').value,
                    pass: document.getElementById('txtClave').value
                }
            }).then(function (result) {
                mostrarMsgValidacion_Login(result.responseText);
                $("#loaderOverlay").hide();
            });
        }
    }
}

function ValidateLogin(val1, val2) {
    var message; var valor = true;
    message = '<ul class="jconfirm-error-list">';
    if (val1 == '') { message += '<li>Ingrese usuario</li>'; valor = false; }
    if (val2 == '') { message += '<li>Ingrese Contraseña.</li>'; valor = false; }
    message += '</u>';
    if (valor) {
        return true;
    } else {
        $.alert({
            title: 'Mensaje',
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            typeAnimated: true,
            content: message,
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
        return false;
    }
}
function mostrarMsgValidacion_Login(rpta) {
    if (rpta == "ACT") {
        var urlRaiz = document.getElementById("hdfRaiz").value;
        var urlBase = "http://" + window.location.host + urlRaiz;
        var url = urlBase + "Main/plantilla";
        window.location.href = url;
    } else {
        $.alert({
            title: 'Mensaje',
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list "><li>' + 'Usuario y Contraseña no registrados' + '</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }
}