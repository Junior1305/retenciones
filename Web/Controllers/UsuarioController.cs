﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class UsuarioController : Controller
    {
        //
        // GET: /Usuario/
        public ActionResult mant_usuario()
        {
            return View();
        }

       

        public string list_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Usuario_Listar", parametro);
            }
            return rpta;
        }

        public string search_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Usuario_Buscar", parametro);
            }
            return rpta;
        }

        public string search_usuario_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Usuario_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Usuario"+ "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if(log != ""){
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_Usuario_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Usuario" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Usuario_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Usuario" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Usuario_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_usuario()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Usuario_Mostrar", parametro);
            }
            return rpta;
        }

    }
}