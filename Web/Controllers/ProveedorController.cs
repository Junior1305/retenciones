﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class ProveedorController : Controller
    {
        // GET: Proveedor
        public ActionResult mant_proveedor()
        {
            return View();
        }
        public string list_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Proveedor_Listar", parametro);
            }
            return rpta;
        }

        public string list_proveedor_cbo()
        {
            string rpta = "";
            _adoSQL odaSQL = new _adoSQL("conexion");
            rpta = odaSQL.executeCommand("usp_tbl_Proveedor_Listar_cbo");
            return rpta;
        }

        public string search_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Proveedor_Buscar", parametro);
            }
            return rpta;
        }

        public string search_proveedor_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Proveedor_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Proveedor" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if (log != "")
                {
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_Proveedor_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Proveedor" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Proveedor_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Proveedor" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Proveedor_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Proveedor_Mostrar", parametro);
            }
            return rpta;
        }

    }
}