﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    [Security.filterSecurity]
    public class MainController : Controller
    {
        // GET: Main
        CookieContainer cokkie = new CookieContainer();
        string captcha = "";
        public ActionResult plantilla()
        {
            return View();
        }

        public string listarMenu()
        {
            string rpta = "";
            _adoSQL odaSQL = new _adoSQL("conexion");
            string idUsuario = "" + Session["IdUsuario"];
            rpta = odaSQL.executeCommandParameter("usp_tbl_OpcionPerfil_Listar", idUsuario);
            return rpta;
        }

        public string ubigeo()
        {
            string rpta = "";
            _adoSQL odaSQL = new _adoSQL("conexion");
            rpta = odaSQL.executeCommand("usp_tbl_Ubigeo_Listar");
            return rpta;
        }



        public string getSunat()
        {
            string rpta = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=random");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                request.CookieContainer = cokkie;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream mystream2 = response.GetResponseStream();
                StreamReader mystreamReader2 = new StreamReader(mystream2);
                string responseText = mystreamReader2.ReadToEnd();

                long n = Request.InputStream.Length;
                if (n > 0)
                {
                    byte[] buffer = new byte[n];
                    Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                    string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                    string[] matriz = parametro.Split('|');
                    string myurl = @"http://e-consultaruc.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRuc&nroRuc=" + matriz[0] + "&numRnd="+ responseText.ToString();
                    HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create(myurl);
                    myWebRequest.CookieContainer = cokkie;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

                    HttpWebResponse myhttpWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                    Stream mystream = myhttpWebResponse.GetResponseStream();
                    StreamReader mystreamReader = new StreamReader(mystream);
                    string xDat = ""; int pos = 0;
                    while (!mystreamReader.EndOfStream)
                    {
                        xDat = mystreamReader.ReadLine();
                        pos++;
                        if (matriz[1] != "10" && matriz[1] != "15" && matriz[1] != "17")
                        {
                            switch (pos)
                            {
                                case 143: // Nombre Comercial
                                    rpta = rpta + getDatafromXML(xDat, 0);
                                    break;
                                case 163: //ESTADO
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 173: //CONDICION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 182: //DIRECCION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                    //default:
                                    //    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    //    break;

                            }
                        }
                        else
                        {
                            switch (pos)
                            {
                                case 154: // Nombre Comerciaal
                                    rpta = rpta + getDatafromXML2(xDat, 0);
                                    break;
                                case 172: //ESTADO
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 182: //CONDICION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                case 184: //DIRECCION
                                    rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    break;
                                    //default:
                                    //        rpta = rpta + "|" + getDatafromXML(xDat, 0);
                                    //        break;

                            }
                        }

                    }

                    return rpta;
                }

            }
            catch (Exception)
            {
                return "Conexion Perdida, Intente de Nuevo.";
            }

            return rpta;
        }

        private string getDatafromXML(string lineRead, int len = 0)
        {
            try
            {
                lineRead = lineRead.Trim();
                lineRead = lineRead.Remove(0, len);
                lineRead = lineRead.Replace("</td>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=1>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=3>", "");
                while (lineRead.Contains("  "))
                {
                    lineRead = lineRead.Replace("  ", " ");
                }
                return lineRead;
            }
            catch (Exception)
            {
                return "-";
            }

        }

        private string getDatafromXML2(string lineRead, int len = 0)
        {
            try
            {
                lineRead = lineRead.Trim();
                lineRead = lineRead.Remove(0, len);
                lineRead = lineRead.Replace("</td>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=1>", "");
                lineRead = lineRead.Replace("<td class=\"bg\" colspan=3>", "");
                lineRead = lineRead.Replace("- ", "");
                while (lineRead.Contains("  "))
                {
                    lineRead = lineRead.Replace("  ", " ");
                }
                return lineRead;
            }
            catch (Exception)
            {
                return "-";
            }

        }

        public string obtenerTipoCambioHtml() {
            string sGetResponse = string.Empty;
            string url = System.Configuration.ConfigurationManager.AppSettings["urlTipoCambio"].ToString();
            string mes, anio;
            if(Request.InputStream != null && Request.InputStream.Length > 0){
                long n = Request.InputStream.Length;
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametros = Encoding.Default.GetString(buffer);
                mes = parametros.Split('|')[0];
                anio = parametros.Split('|')[1];
                if(mes != "Mes" && anio != "Anho")
                {
                    url += "?mes=" + mes + "&anho=" + anio;
                }
                
            }

            Encoding objenco = Encoding.GetEncoding("ISO-8859-1");
            WebProxy objWebProxy = new WebProxy("proxy", 80);
            CookieCollection objCookie  = new CookieCollection();
            HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create(url);

            myWebRequest.Credentials = CredentialCache.DefaultCredentials;
            myWebRequest.ProtocolVersion = HttpVersion.Version11;
            myWebRequest.UserAgent = ".NET Framework 4.0";
            myWebRequest.Method = "GET";

            myWebRequest.CookieContainer = new CookieContainer();
            myWebRequest.CookieContainer.Add(objCookie);

            try
            {
                HttpWebResponse myhttpWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream mystream = myhttpWebResponse.GetResponseStream();
                StreamReader mystreamReader = new StreamReader(mystream,objenco,true);
                sGetResponse = mystreamReader.ReadToEnd();
            }
            catch (Exception)
            {
                obtenerTipoCambioHtml();
            }

            return sGetResponse;
        }

        public string cargarListaSunat()
        {
            string url, mes="", anio="";
            string rptaCompleta = ""; string rptaGeneral = "";
            url = "cl-at-ittipcam/tcS01Alias";
            if (Request.InputStream != null && Request.InputStream.Length > 0)
            {
                long n = Request.InputStream.Length;
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametros = Encoding.Default.GetString(buffer);
                mes = parametros.Split('|')[1];
                anio = parametros.Split('|')[0];
                if (mes != "Mes" && anio != "Anho")
                {
                    url += "?mes=" + mes + "&anho=" + anio;
                }

            }
            HttpClient cliente = new HttpClient();
            cliente.BaseAddress = new Uri("http://www.sunat.gob.pe/");
            HttpResponseMessage rpta = cliente.GetAsync(url).Result;
            if (rpta != null && rpta.IsSuccessStatusCode)
            {
                string contenido = "";
                using (MemoryStream ms = (MemoryStream)rpta.Content.ReadAsStreamAsync().Result)
                {
                    byte[] buffer = ms.ToArray();
                    contenido = Encoding.UTF8.GetString(buffer);
                    contenido = contenido.ToLower();
                }
                if (contenido.Length > 0)
                {
                    int posInicioT1 = contenido.IndexOf("<table");
                    int posFinT1 = contenido.IndexOf("</table");
                    if (posInicioT1 > -1 && posFinT1 > -1)
                    {
                        int posInicioT2 = contenido.IndexOf("<table", posInicioT1 + 1);
                        int posFinT2 = contenido.IndexOf("</table", posFinT1 + 1);
                        string tabla = contenido.Substring(posInicioT2, posFinT2 - posInicioT2 + 8);
                        posInicioT1 = 0;
                        tabla = tabla.Replace("</strong>", "");
                        List<string> valores = new List<string>();
                        int count = 3;
                        for (int i = 1; i < 100; i++)
                        {
                            posInicioT1 = tabla.LastIndexOf("</td");
                            if (posInicioT1 > -1)
                            {
                                tabla = tabla.Substring(0, posInicioT1).Trim();
                                posFinT1 = tabla.LastIndexOf(">");
                                if (posFinT1 > -1)
                                {
                                    if (count == i)
                                    {
                                        rptaCompleta += tabla.Substring(posFinT1 + 1, tabla.Length - posFinT1 - 1).Trim() + "*";
                                        count = count + 3;
                                    }
                                    else
                                    {
                                        rptaCompleta += tabla.Substring(posFinT1 + 1, tabla.Length - posFinT1 - 1).Trim() + "|";
                                    }
                                }
                            }
                        }
                        rptaCompleta = rptaCompleta.Substring(0, rptaCompleta.Length - 69);
                        _adoSQL odaSQL = new _adoSQL("conexion");
                        string idUsuario = rptaCompleta + "¥" + mes + "¥" + anio + "¥" + Session["Usuario"];
                        rptaGeneral = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Cargar", idUsuario);
                    }
                }
            }
            return rptaGeneral;
        }
    }
    
}