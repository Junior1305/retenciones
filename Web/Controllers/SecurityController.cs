﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class SecurityController : Controller
    {
        // GET: Security
        public ActionResult LogIn()
        {
            return View();
        }

        public string validarLogin()
        {
            string rpta = string.Empty;
            string rpta2 = string.Empty;
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string[] matriz2 = Server.UrlDecode(matriz[0]).Split('|');
                
                //LDAP rp = new LDAP("LDAP://192.168.1.1/CN=Users,dc=laz,dc=bvcorp,dc=corp");
                //if (rp.autenticado("laz.bvcorp.corp", matriz2[0], matriz2[1]))
                //{
                    _adoSQL odaSQL = new _adoSQL("conexion");
                    //string Datos = matriz2[0];
                    string log = matriz[1] + "¥" + "4" + "¥" + "Login" + "¥" + matriz2[0];
                    rpta2 = odaSQL.executeCommandParameterLog("usp_tbl_Usuario_login", matriz[0],log);
                    if (rpta2 != "0")
                    {

                        rpta = "ACT";
                        Session["Usuario"] = matriz2[0];
                        Session["IdUsuario"] = rpta2;
                    }
                    else
                    {
                        rpta = "Usuario y Password no registrados";
                    }
                ////}
                //else
                //{
                //    rpta = "Usuario y Password no registrados";
                //}
            }
            return rpta;
        }

        public string Logout()
        {
            string rpta = "";

            Session.Clear();
            Session.Abandon();

            rpta = "../";

            return rpta;
        }

    }
}