﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using DataAccess;

namespace Web.Controllers
{
    public class TipoRetencionController : Controller
    {
        //
        // GET: /TipoRetencion/
        public ActionResult mant_tipoRetencion()
        {
            return View();
        }

        public string list_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_Listar", parametro);
            }
            return rpta;
        }

        public string list_tipoRetencion_cbo()
        {
            string rpta = "";
            _adoSQL odaSQL = new _adoSQL("conexion");
            string ado = "" + Session["IdUsuario"];
            rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_Listar_cbo",ado);
            return rpta;
        }

        public string get_correlativoTipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_ObtenerCorrelativo", parametro);
            }
            return rpta;
        }

        public string search_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_Buscar", parametro);
            }
            return rpta;
        }

        public string search_tipoRetencion_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Tipo Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if (log != "")
                {
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoRetencion_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Tipo Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoRetencion_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Tipo Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoRetencion_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_tipoRetencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoRetencion_Mostrar", parametro);
            }
            return rpta;
        }
    }
}