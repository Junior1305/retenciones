﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class RetencionController : Controller
    {
        //
        // GET: /Retencion/
        public ActionResult mant_retencion()
        {
            return View();
        }

        public string list_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Listar", parametro);
            }
            return rpta;
        }
 
        public string search_proveedor()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_ProveedorRetencion_Buscar", parametro);
            }
            return rpta;
        }

        public string search_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Buscar", parametro);
            }
            return rpta;
        }

        public string search_retencion_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if (log != "")
                {
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_Retencion_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Retencion_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Retencion" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Retencion_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_retencion()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Mostrar", parametro);
            }
            return rpta;
        }

        public string web_service()
        {
            string rpta = ""; string rpta2 = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Mostrar_WS", parametro);
                string[] Retencion = rpta.Split('|');
                decimal tipo_Cambio = Math.Round(decimal.Parse(Retencion[9]),3);
                WSPrueba.RET_CABECERA_BE Cabecera = new WSPrueba.RET_CABECERA_BE();
                Cabecera.COD_MND = "PEN";
                Cabecera.COD_TIP_RET = "20";
                Cabecera.NUM_SERIE_RET = Retencion[2];
                Cabecera.NUM_CORRE_RET = Retencion[3];
                Cabecera.FEC_EMIS_RET = Retencion[7];
                Cabecera.COD_TIP_NIF_EMIS = "6";
                if(Retencion[2] == "R001"){
                    Cabecera.NUM_NIF_EMIS = "20101087566";
                    Cabecera.NOM_COMER_EMIS = "BUREAU VERITAS DEL PERU S.A.";
                    Cabecera.COD_UBI_EMIS = "150131";
                    Cabecera.NOM_RZN_SOC_EMIS = "BUREAU VERITAS DEL PERU S.A.";
                    Cabecera.TXT_DMCL_FISC_EMIS = "AV. CAMINO REAL NRO. 390 INT. 1402 URB. CENTRO COMERCIAL CAMINO R LIMA - LIMA - SAN ISIDRO";
                }else{
                    Cabecera.NUM_NIF_EMIS = "20385739771";
                    Cabecera.NOM_COMER_EMIS = "-";
                    Cabecera.COD_UBI_EMIS = "070101";
                    Cabecera.NOM_RZN_SOC_EMIS = "INSPECTORATE SERVICES PERU S.A.C.";
                    Cabecera.TXT_DMCL_FISC_EMIS = "AV. ELMER FAUCETT NRO. 444 PROV. CONST. DEL CALLAO - PROV. CONST. DEL CALLAO - CALLAO";
                }
                Cabecera.TXT_PAIS_EMIS = "PE";
                Cabecera.NOM_RZN_SOC_RECP = Retencion[5];
                Cabecera.COD_TIP_NIF_RECP = "6";
                Cabecera.NUM_NIF_RECP = Retencion[4];
                Cabecera.TXT_DMCL_FISC_RECP = Retencion[16];
                Cabecera.COD_UBI_RECP = Retencion[6];
                Cabecera.COD_REG_RET = "01";
                Cabecera.TASA = "3.00";
                Cabecera.MNT_TOT_RET = Retencion[11];
                Cabecera.COD_MND_RET = "PEN";
                Cabecera.MNT_TOT_PAG = Retencion[10];
                Cabecera.COD_MND_PAG = "PEN";
                Cabecera.COD_FORM_IMPR = "001";
                Cabecera.TXT_CORREO_ENVIO = Retencion[13];
                Cabecera.TXT_CORREO_OCULTO = Retencion[14];
                
                List<WSPrueba.RET_DETALLE_BE> Detalle = new List<WSPrueba.RET_DETALLE_BE>();
                WSPrueba.RET_DETALLE_BE valor;
                string[] DetalleRetencion = Retencion[15].Split('^');
                int count = 0;
                foreach(string value in DetalleRetencion){
                    count++;
                    string[] Detalle2 = value.Split('¬');
                    decimal mnt_tot_rec = Math.Round(decimal.Parse(Detalle2[9]),2);
                    decimal mnt_sin_ret_pag = Math.Round(decimal.Parse(Detalle2[7]), 2);
                    valor = new WSPrueba.RET_DETALLE_BE();
                    valor.NUM_LIN_ITEM = count.ToString();
                    valor.COD_TIP_REC = Detalle2[1];
                    valor.NUM_SERIE_REC = Detalle2[2];
                    valor.NUM_CORRE_REC = Detalle2[3];
                    valor.FEC_EMIS_REC = Detalle2[4];
                    
                    if (Retencion[8] == "1")
                    {
                        valor.TIP_MND_REC = "PEN";
                        valor.MNT_TOT_REC = Detalle2[9];
                        valor.MNT_SIN_RET_PAG = Detalle2[7];
                        valor.COD_MND_PAG = "PEN";
                        valor.COD_MND_REF_TIC = "PEN";
                        valor.COD_MND_OBJ_TIC = "PEN";
                        valor.TIP_CAM = "1.000";
                        valor.FEC_TIP_CAM = Retencion[7];
                    }
                    else
                    {
                        valor.TIP_MND_REC = "USD";
                        valor.MNT_TOT_REC = Math.Round((mnt_tot_rec / tipo_Cambio),2).ToString();
                        valor.MNT_SIN_RET_PAG = Math.Round((mnt_sin_ret_pag / tipo_Cambio), 2).ToString();
                        valor.COD_MND_PAG = "USD";
                        valor.COD_MND_REF_TIC = "USD";
                        valor.COD_MND_OBJ_TIC = "PEN";
                        valor.TIP_CAM = decimal.Parse(Retencion[9]).ToString("N4");
                        valor.FEC_TIP_CAM = Retencion[7];

                    }
                    valor.FEC_PAG = Detalle2[4];
                    valor.NRO_PAG = Detalle2[8];
                    
                    valor.MNT_RET = Detalle2[6];
                    valor.COD_MND_RET = "PEN";
                    valor.FEC_RET = Detalle2[4];
                    valor.MNT_TOT_NET = Detalle2[5];
                    valor.COD_MND_TOT_NET = "PEN";
                    valor.SERIE_CORRE_CPE_RET = Detalle2[2] + "-" + Detalle2[3];
                    Detalle.Add(valor);
                }
                
                WSPrueba.RET_RESPUESTA_BE orespueta = new WSPrueba.RET_RESPUESTA_BE();

                WSPrueba.MercuryWebRet oNuevoServices = new WSPrueba.MercuryWebRet();
                orespueta = oNuevoServices.SendRET("Gocrospoma", "Gerson@001", Cabecera, Detalle.ToArray(),null,null,"");
                
                _adoSQL odaSQL2 = new _adoSQL("conexion");
                string matriz = "";
                string repetido = "el recibo ya existe en la BD";
                switch (orespueta.CODIGO)
                {
                    case "001":
                         matriz = parametro + '¥' + "Procesado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "002":
                        if (orespueta.DETALLE.Contains(repetido))
                        {
                            matriz = parametro + '¥' + "Procesado" + '¥' + "001" + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        }
                        else {
                            matriz = parametro + '¥' + "Error Data" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        }
                        break;
                    case "003":
                         matriz = parametro + '¥' + "Error Tecnico" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    default:
                        matriz = parametro + '¥' + "Otros" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                }
                
                rpta2 = odaSQL2.executeCommandParameter("usp_tbl_Retencion_ActualizarEstado1", matriz);
            }
            return rpta2;
        }

        public string web_service_info()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_MostrarInfoWS", parametro);
            }
            return rpta;
        }

        public string web_service_txt()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Mostrar_TXT", parametro);
            }
            return rpta;
        }

        public string web_service_baja()
        {
            string rpta = ""; string rpta2 = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('|');
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Mostrar_WS", matriz[0]);
                string[] Retencion = rpta.Split('|');

                List<WSPrueba2.CPE_DOC_BAJA> Documento = new List<WSPrueba2.CPE_DOC_BAJA>();

                WSPrueba2.CPE_DOC_BAJA valor = new WSPrueba2.CPE_DOC_BAJA();
                if (Retencion[2] == "R001")
                {
                    valor.NUM_NIF_EMIS = "20101087566";
                    valor.COD_TIP_NIF_EMIS = "6";
                    valor.COD_TIP_CPE = "20";
                    valor.FEC_EMIS = Retencion[7];
                    valor.NUM_SERIE_CPE = Retencion[2];
                    valor.NUM_CORRE_CPE = Retencion[3];
                    valor.TXT_MTVO_BAJA = matriz[1];
                }
                else
                {
                    valor.NUM_NIF_EMIS = "20385739771";
                    valor.COD_TIP_NIF_EMIS = "6";
                    valor.COD_TIP_CPE = "20";
                    valor.FEC_EMIS = Retencion[7];
                    valor.NUM_SERIE_CPE = Retencion[2];
                    valor.NUM_CORRE_CPE = Retencion[3];
                    valor.TXT_MTVO_BAJA = matriz[1];
                }
                Documento.Add(valor);

                List<WSPrueba2.RET_RESPUESTA_BE> lrespueta = new List<WSPrueba2.RET_RESPUESTA_BE>();
                WSPrueba2.wsOperatividadRET oNuevoServices = new WSPrueba2.wsOperatividadRET();
                lrespueta = oNuevoServices.LowCER("Gocrospoma", "Gerson@001", Documento.ToArray()).ToList();
                _adoSQL odaSQL2 = new _adoSQL("conexion");
                string RptaGeneral = "";
                switch (lrespueta[0].CODIGO)
                {
                    case "201":
                        RptaGeneral = matriz[0] + '¥' + "Solicitud de Baja" + '¥' + matriz[1] + '¥' + matriz[1] + '¥' + lrespueta[0].DETALLE;
                        break;
                    case "202":
                        RptaGeneral = matriz[0] + '¥' + "Error Anulacion" + '¥' + lrespueta[0].CODIGO + '¥' + matriz[1] + '¥' + lrespueta[0].DETALLE;
                        break;
                    case "203":
                        RptaGeneral = matriz[0] + '¥' + "Error Anulacion" + '¥' + lrespueta[0].CODIGO + '¥' + matriz[1] + '¥' + lrespueta[0].DETALLE;
                        break;
                    default:
                        RptaGeneral = matriz[0] + '¥' + "Error Anulacion" + '¥' + lrespueta[0].CODIGO + '¥' + matriz[1] + '¥' + lrespueta[0].DETALLE;
                        break;
                }

                rpta2 = odaSQL2.executeCommandParameter("usp_tbl_Retencion_ActualizarEstado2", RptaGeneral);
            }
            return rpta2;
        }

        public string web_service_consulta()
        {
            string rpta = ""; string rpta2 = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('|');
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Retencion_Mostrar_WS", matriz[0]);
                string[] Retencion = rpta.Split('|');

                
                WSPrueba2.RET_RESPUESTA_BE orespueta = new WSPrueba2.RET_RESPUESTA_BE();
                WSPrueba2.wsOperatividadRET oNuevoServices = new WSPrueba2.wsOperatividadRET();
                if(Retencion[2] == "R001"){
                    orespueta = oNuevoServices.ReadCER("Gocrospoma", "Gerson@001",Retencion[2],Retencion[3],"20","20101087566");
                }else{
                    orespueta = oNuevoServices.ReadCER("Gocrospoma", "Gerson@001", Retencion[2], Retencion[3], "20", "20385739771");
                }
                
                _adoSQL odaSQL2 = new _adoSQL("conexion");
                string RptaGeneral = "";
                switch (orespueta.CODIGO)
                {
                    case "101":
                        RptaGeneral = matriz[0] + '¥' + "Procesado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "102":
                        RptaGeneral = matriz[0] + '¥' + "Aceptado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "103":
                        RptaGeneral = matriz[0] + '¥' + "Aceptado con observaciones" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "104":
                        RptaGeneral = matriz[0] + '¥' + "Rechazado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "105":
                        RptaGeneral = matriz[0] + '¥' + "Anulado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "108":
                        RptaGeneral = matriz[0] + '¥' + "Solicitud de Baja" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    case "110":
                        RptaGeneral = matriz[0] + '¥' + "Pendiente" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                    default:
                        RptaGeneral = matriz[0] + '¥' + "Procesado" + '¥' + orespueta.CODIGO + '¥' + orespueta.DESCRIPCION + '¥' + orespueta.DETALLE;
                        break;
                }

                rpta2 = odaSQL2.executeCommandParameter("usp_tbl_Retencion_ActualizarEstado3", RptaGeneral);
            }
            return rpta2;
        }
    }
}