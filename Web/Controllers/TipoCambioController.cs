﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class TipoCambioController : Controller
    {
        // GET: TipoCambio
        public ActionResult mant_tipoCambio()
        {
            return View();
        }

        public string list_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Listar", parametro);
            }
            return rpta;
        }

        public string search_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Buscar", parametro);
            }
            return rpta;
        }

        public string search_tipoCambio_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Tipo Cambio" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if (log != "")
                {
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoCambio_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Tipo Cambio" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoCambio_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Tipo Cambio" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_TipoCambio_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_tipoCambio()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Mostrar", parametro);
            }
            return rpta;
        }

        public string get_TipoCambio_date()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_TipoCambio_Obtener", parametro);
            }
            return rpta;
        }
    }
}