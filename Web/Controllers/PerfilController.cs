﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class PerfilController : Controller
    {
        //
        // GET: /Perfil/
        public ActionResult mant_perfil()
        {
            return View();
        }

        public string list_perfil_cbo()
        {
            string rpta = "";
            _adoSQL odaSQL = new _adoSQL("conexion");
            rpta = odaSQL.executeCommand("usp_tbl_Perfil_Listar_cbo");
            return rpta;
        }

        public string list_treeView()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_PerfilTreeView_Listar", parametro);
            }
            return rpta;
        }

        public string list_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Perfil_Listar", parametro);
            }
            return rpta;
        }
        public string search_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Perfil_Buscar", parametro);
            }
            return rpta;
        }

        public string search_perfil_id()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Perfil_Buscar_id", parametro);
            }
            return rpta;
        }

        public string new_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "1" + "¥" + "Perfil" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                if (log != "")
                {
                    rpta = odaSQL.executeCommandParameterLog("usp_tbl_Perfil_Insertar", matriz[0], log);
                }
            }
            return rpta;
        }

        public string update_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "2" + "¥" + "Perfil" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Perfil_Actualizar", matriz[0], log);
            }
            return rpta;
        }

        public string delete_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                string[] matriz = parametro.Split('¥');
                string log = matriz[1] + "¥" + "3" + "¥" + "Perfil" + "¥" + Session["Usuario"];
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameterLog("usp_tbl_Perfil_Eliminar", matriz[0], log);
            }
            return rpta;
        }

        public string show_perfil()
        {
            string rpta = "";
            long n = Request.InputStream.Length;
            if (n > 0)
            {
                byte[] buffer = new byte[n];
                Request.InputStream.Read(buffer, 0, System.Convert.ToInt32(n));
                string parametro = Server.UrlDecode(Encoding.Default.GetString(buffer));
                _adoSQL odaSQL = new _adoSQL("conexion");
                rpta = odaSQL.executeCommandParameter("usp_tbl_Perfil_Mostrar", parametro);
            }
            return rpta;
        }
    }
}