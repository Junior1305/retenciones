﻿function loadMantProveedor(urlView, urlLoading, idContent) {
    $("#loaderOverlay").show();
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", urlView);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("contenido").innerHTML = xmlhttp.responseText;
            document.getElementById("contenido").style.display = "block";
            sendServer({
                method: "POST",
                url: "../Proveedor/list_proveedor",
                params: {
                    razonSocial: "",
                    ruc: "",
                    estado: 0
                }
            }).then(function (result) {
                armarlistarProveedor(result.responseText);
                return sendServer({
                    method: "GET",
                    url: "../Main/ubigeo"
                }).then(function (result) {
                    mostrarListasUbigeoProveedor(result.responseText);
                    $('[data-toogle="tooltip"]').tooltip();
                    $("#loaderOverlay").hide();
                });

            });


            document.getElementById("selectDept").onchange = function () {
                sendServer({
                    method: "GET",
                    url: "../Main/ubigeo"
                }).then(function (result) {
                    mostrarListaProvinciaProveedor(result.responseText);
                    return sendServer({
                        method: "GET",
                        url: "../Main/ubigeo"
                    }).then(function (result) {
                        mostrarListaDistritoProveedor(result.responseText);
                    });
                });
            }

            document.getElementById("selectProv").onchange = function () {
                sendServer({
                    method: "GET",
                    url: "../Main/ubigeo"
                }).then(function (result) {
                    mostrarListaDistritoProveedor(result.responseText);
                });
            }

            document.getElementById("txtRUC").onkeypress = function (e) {
                if (event.which == 13 || event.keyCode == 13) {
                    BuscarSunat();
                }
            }
        }
    }
}

function BuscarSunat() {
    $("#loaderOverlay").show();
    var tipo = document.getElementById('txtRUC').value;
    sendServer({
        method: "GET",
        url: "../Main/getSunat",
        params: {
            ruc: document.getElementById('txtRUC').value,
            tipo: tipo.substr(0, 2)
        }
    }).then(function (result) {
        var data = result.responseText;
        var matriz = data.split('|');
        if (data.length > 0) {
            if (tipo.substr(0, 2) != '10' && tipo.substr(0, 2) != '15' && tipo.substr(0, 2) != '17') {
                document.getElementById('txtRazonSocial').value = matriz[0].substr(39,matriz[0].length);
                document.getElementById('txtEstado').value = matriz[1];
                document.getElementById('txtCondicion').value = matriz[2];
                document.getElementById('txtDireccion').value = matriz[3];
                var matriz = matriz[3].split('-');
                var Dept = matriz[0].replace('PROV. CONST. DEL ','').trim();
                var Prov = matriz[1].trim();
                var Dist = matriz[2].trim();
                $("#selectDept option").each(function (index) {
                    if (Dept.indexOf($(this).text()) != -1) {
                        $("#selectDept").find('option:contains("' + $(this).text() + '")').prop('selected', true);
                    }
                });
                
                ListarProvinciaViewProveedorByText(Prov);
                ListarDistritoViewProveedorByText(Dist);
            } else {
                document.getElementById('txtRazonSocial').value = matriz[0];
                document.getElementById('txtEstado').value = matriz[1];
                document.getElementById('txtCondicion').value = matriz[2];
                document.getElementById('txtDireccion').value = matriz[3];
                $("#selectDept").find('option:contains("---")').prop('selected', true);
                $("#selectProv").find('option:contains("---")').prop('selected', true);
                $("#selectDist").find('option:contains("---")').prop('selected', true);
            }

        } else {
            $("#loaderOverlay").hide();
            $.alert({
                closeIcon: true,
                boxWidth: '25%',
                useBootstrap: false,
                title: 'Mensaje',
                typeAnimated: true,
                content: '<ul class="jconfirm-error-list"><li>Conexion Perdida, Intente de Nuevo.</li><li>En caso de que persista, espere unos 10 min para realizar la consulta a la sunat.</li></ul>',
                buttons: {
                    Aceptar: {
                        btnClass: 'btn any-other-class'
                    }
                }
                
            });
        }
        $("#loaderOverlay").hide();
    });
}

function mostrarListasUbigeoProveedor(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarDepartamentoProveedor(lista);
    }
}

function listarDepartamentoProveedor(lista) {
    var dptos = [];
    var nRegistros = lista.length;
    var campos;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (campos[2] == "00" && campos[3] == "00") {
            dptos.push(campos[1] + "|" + campos[4]);
        }
    }
    crearCombo(dptos, "selectDept", "---")
}

function mostrarListaProvinciaProveedor(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarProvinciaProveedor(lista);
    }
}

function listarProvinciaProveedor(lista) {
    var provs = [];
    var nRegistros = lista.length;
    var campos;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idDpto == campos[1] && campos[2] != "00" && campos[3] == "00") {
            provs.push(campos[2] + "|" + campos[4]);
        }
    }
    crearCombo(provs, "selectProv", "---")
}

function mostrarListaDistritoProveedor(rpta) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarDistritoProveedor(lista);
    }
}

function listarDistritoProveedor(lista) {
    var dist = [];
    var nRegistros = lista.length;
    var campos;
    var idProv = document.getElementById("selectProv").value;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idProv == campos[2] && campos[1] == idDpto && campos[3] != "00") {
            dist.push(campos[3] + "|" + campos[4]);
        }
    }
    crearCombo(dist, "selectDist", "---")
}

function nuevoProveedor() {
    document.getElementById("modalMessageTitle").innerHTML = "Nuevo Proveedor";
    $('#modalProveedor').modal(mShow);
    insertInput('modalProveedor');
}

function armarlistarProveedor(rpta) {
    $('.numeric').on('keypress', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    if (document.getElementById('tblListaProveedor').firstElementChild != null) {
        $("#tblListaProveedor").dataTable().fnDestroy();
    }

    if (rpta != "") {
        var contenido = "";
        var registros = rpta.split("*");
        var campo;
        var dataSet = [];
        var option = "<a onclick=\"EliminarProveedor(this);\" class=\"btn btn-danger btn-table\" title=\"Eliminar\" data-toogle=\"tooltip\" data-placement=\"top\" ><i class=\"fa fa-trash-o\"></i></a> <a onclick=\"VisualizarProveedor(this);\" class=\"btn btn-primary btn-table\" title=\"Ver\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-eye\"></i></a> <a onclick=\"EditarProveedor(this);\" class=\"btn btn-primary btn-table\" title=\"Editar\" data-toogle=\"tooltip\" data-placement=\"top\"><i class=\"fa fa-pencil-square-o\"></i></a>";
        for (var i = 0; i < registros.length; i++) {
            var Valores = [];
            campo = registros[i].split("|");
            for (var j = 0; j < campo.length; j++) {
                Valores.push(campo[j]);
            }
            Valores.push(option);
            dataSet.push(Valores);
        }
        $('#tblListaProveedor').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Proveedor" },
                { title: "RUC" },
                { title: "Razon Social" },
                { title: "Direccion" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [5],
                    "width": "14%"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });
    } else {
        document.getElementById("tblListaProveedor").innerHTML = "";

        $('#tblListaProveedor').DataTable({
            data: dataSet,
            columns: [
                { title: "Id Proveedor" },
                { title: "RUC" },
                { title: "Razon Social" },
                { title: "Direccion" },
                { title: "Fecha Registro" },
                { title: "Opciones" }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    className: "hide_column"
                },
                {
                    "targets": [0],
                    "searchable": false
                },
                {
                    "targets": [5],
                    "width": "14%"
                }
            ],
            dom: "<'row'<'col-sm-6'f><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-3'l><'col-sm-9'p>>"
        });

    }
}

function AccionProveedor(ind) {
    if (ind == 'I') {
        if (validateInputs('modalProveedor')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Proveedor/search_proveedor",
                params: {
                    ruc: document.getElementById('txtRUC').value,
                }
            }).then(function (result) {
                armarSearchProveedor(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        if (validateInputs('modalProveedor')) {
            $("#loaderOverlay").show();
            sendServer({
                method: "POST",
                url: "../Proveedor/search_proveedor_id",
                params: {
                    id: document.getElementById('proveedorIDHid').value,
                    ruc: document.getElementById('txtRUC').value
                }
            }).then(function (result) {
                armarSearchProveedor(result.responseText, ind);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    }
}

function armarSearchProveedor(rpta, ind) {
    if (rpta == '0') {
        if (ind == 'I') {
            sendServerLog({
                method: "POST",
                url: "../Proveedor/new_proveedor",
                params: {
                    ruc: document.getElementById('txtRUC').value,
                    razonSocial: document.getElementById('txtRazonSocial').value.toUpperCase(),
                    direccion: document.getElementById('txtDireccion').value.toUpperCase(),
                    email: document.getElementById('txtEmail').value.toUpperCase(),
                    telefono: document.getElementById('txtTelefono').value,
                    estado_s: document.getElementById('txtEstado').value.toUpperCase(),
                    condicion: document.getElementById('txtCondicion').value.toUpperCase(),
                    departamento: document.getElementById('selectDept').value,
                    provincia: document.getElementById('selectProv').value,
                    distrito: document.getElementById('selectDist').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalProveedor').modal(mHide);
                armarCondProveedor(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        } else {
            sendServerLog({
                method: "POST",
                url: "../Proveedor/update_proveedor",
                params: {
                    id: document.getElementById('proveedorIDHid').value,
                    ruc: document.getElementById('txtRUC').value,
                    razonSocial: document.getElementById('txtRazonSocial').value.toUpperCase(),
                    direccion: document.getElementById('txtDireccion').value.toUpperCase(),
                    email: document.getElementById('txtEmail').value.toUpperCase(),
                    telefono: document.getElementById('txtTelefono').value,
                    estado_s: document.getElementById('txtEstado').value.toUpperCase(),
                    condicion: document.getElementById('txtCondicion').value.toUpperCase(),
                    departamento: document.getElementById('selectDept').value,
                    provincia: document.getElementById('selectProv').value,
                    distrito: document.getElementById('selectDist').value,
                    estado: getRadioValue('estado'),
                    user: document.getElementById('user_session').innerHTML
                }
            }).then(function (result) {
                $("#loaderOverlay").hide();
                $('#modalProveedor').modal(mHide);
                armarCondProveedor(result.responseText);
            }).catch(function (error) {
                console.log('Ocurrió un error: ', error);
            });
        }
    } else {
        $.alert({
            closeIcon: true,
            boxWidth: '25%',
            useBootstrap: false,
            title: 'Mensaje',
            typeAnimated: true,
            content: '<ul class="jconfirm-error-list"><li>Proveedor existente, Verifique en la lista de proveedores.</li><li>En caso de que el error persista, contactarse al area de Soporte.</li></ul>',
            buttons: {
                Aceptar: {
                    btnClass: 'btn any-other-class'
                }
            }
        });
    }

}

function armarCondProveedor(rpta) {
    $.alert({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>' + rpta + '</li></ul>',
        buttons: {
            Aceptar: {
                btnClass: 'btn any-other-class'
            }
        },
        onClose: function () {
            nuevo('../Proveedor/mant_proveedor', 'Proveedor', '1', 'loadMantProveedor');
        }
    });
}

function ClearProveedor() {
    clearInput('FormularioProveedor');
    filtrarListaProveedor();
}

function filtrarListaProveedor() {
    $("#loaderOverlay").show();
    sendServer({
        method: "POST",
        url: "../Proveedor/list_proveedor",
        params: {
            razonSocial: document.getElementById('txtRazonSocial_search').value,
            ruc: document.getElementById('txtRUC_search').value,
            estado: document.getElementById('selectEstado_search').value
        }
    }).then(function (result) {
        armarlistarProveedor(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function EliminarProveedor(elemento) {
    var row = elemento.parentNode.parentNode;
    $.confirm({
        closeIcon: true,
        boxWidth: '25%',
        useBootstrap: false,
        title: 'Mensaje',
        typeAnimated: true,
        content: '<ul class="jconfirm-error-list"><li>Estas seguro de Eliminar el proveedor?</li></ul>',
        buttons: {
            Aceptar: function () {
                $("#loaderOverlay").show();
                sendServerLog({
                    method: "POST",
                    url: "../Proveedor/delete_proveedor",
                    params: {
                        id: row.children[0].innerHTML
                    }
                }).then(function (result) {
                    $.alert({
                        closeIcon: true,
                        boxWidth: '25%',
                        useBootstrap: false,
                        title: 'Mensaje',
                        typeAnimated: true,
                        content: '<ul class="jconfirm-error-list"><li>' + result.responseText + '</li></ul>',
                        buttons: {
                            Aceptar: {
                                btnClass: 'btn any-other-class'
                            }
                        },
                        onClose: function () {
                            return sendServer({
                                method: "POST",
                                url: "../Proveedor/list_proveedor",
                                params: {
                                    razonSocial: document.getElementById('txtRazonSocial_search').value,
                                    ruc: document.getElementById('txtRUC_search').value,
                                    estado: document.getElementById('selectEstado_search').value
                                }
                            }).then(function (result) {
                                armarlistarProveedor(result.responseText);
                                $("#loaderOverlay").hide();
                            });
                        }
                    });
                });
            },
            Cancelar: function () {

            }
        }
    });
}

function VisualizarProveedor(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Visualizar Proveedor";
    $('#modalProveedor').modal(mShow);
    viewInput('modalProveedor');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Proveedor/show_proveedor",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarProveedor(result.responseText);
        $("#loaderOverlay").hide();
    });
}

function armarProveedor(rpta) {
    var data = rpta.split("|");
    document.getElementById('proveedorIDHid').value = data[0];
    document.getElementById('txtRUC').value = data[1];
    document.getElementById('txtRazonSocial').value = data[2];
    document.getElementById('txtDireccion').value = data[3];
    document.getElementById('txtEmail').value = data[4];
    document.getElementById('txtTelefono').value = data[5];
    document.getElementById('txtEstado').value = data[6];
    document.getElementById('txtCondicion').value = data[7];
    document.getElementById('selectDept').value = data[8];
    ListarProvinciaViewProveedor(data[9]);
    ListarDistritoViewProveedor(data[10]);
    putRadioValue('estado', data[11]);
}

function EditarProveedor(elemento) {
    $("#loaderOverlay").show();
    document.getElementById("modalMessageTitle").innerHTML = "Editar Proveedor";
    $('#modalProveedor').modal(mShow);
    editInput('modalProveedor');
    var row = elemento.parentNode.parentNode;
    sendServer({
        method: "POST",
        url: "../Proveedor/show_proveedor",
        params: {
            id: row.children[0].innerHTML
        }
    }).then(function (result) {
        armarProveedor(result.responseText);
        $("#loaderOverlay").hide();
    });
}



function ListarProvinciaViewProveedor(value) {
    sendServer({
        method: "GET",
        url: "../Main/ubigeo",
    }).then(function (result) {
        mostrarListaProvinciaProveedor(result.responseText, value);
        $("#loaderOverlay").hide();
    });
}

function mostrarListaProvinciaProveedor(rpta, value) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarProvinciaProveedor(lista, value);
    }
}
function listarProvinciaProveedor(lista, value) {
    var provs = [];
    var nRegistros = lista.length;
    var campos;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idDpto == campos[1] && campos[2] != "00" && campos[3] == "00") {
            provs.push(campos[2] + "|" + campos[4]);
        }
    }
    crearCombo(provs, "selectProv", "---")
    document.getElementById("selectProv").value = value;

}

function ListarDistritoViewProveedor(value) {
    sendServer({
        method: "GET",
        url: "../Main/ubigeo",
    }).then(function (result) {
        mostrarListaDistritoProveedor(result.responseText, value);
        $("#loaderOverlay").hide();
    });
}

function mostrarListaDistritoProveedor(rpta, value) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarDistritoProveedor(lista, value);
    }
}
function listarDistritoProveedor(lista, value) {
    var dist = [];
    var nRegistros = lista.length;
    var campos;
    var idProv = document.getElementById("selectProv").value;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idProv == campos[2] && campos[1] == idDpto && campos[3] != "00") {
            dist.push(campos[3] + "|" + campos[4]);
        }
    }
    crearCombo(dist, "selectDist", "---")
    document.getElementById("selectDist").value = value;
}

function ListarProvinciaViewProveedorByText(value) {
    sendServer({
        method: "GET",
        url: "../Main/ubigeo",
    }).then(function (result) {
        mostrarListaProvinciaProveedorByText(result.responseText, value);
        $("#loaderOverlay").hide();
    });
}

function mostrarListaProvinciaProveedorByText(rpta, value) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarProvinciaProveedorByText(lista, value);
    }
}
function listarProvinciaProveedorByText(lista, value) {
    var provs = [];
    var nRegistros = lista.length;
    var campos;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idDpto == campos[1] && campos[2] != "00" && campos[3] == "00") {
            provs.push(campos[2] + "|" + campos[4]);
        }
    }
    crearCombo(provs, "selectProv", "---")
    $("#selectProv").find('option:contains("' + value + '")').prop('selected', true);
}

function ListarDistritoViewProveedorByText(value) {
    sendServer({
        method: "GET",
        url: "../Main/ubigeo",
    }).then(function (result) {
        mostrarListaDistritoProveedorByText(result.responseText, value);
        $("#loaderOverlay").hide();
    });
}

function mostrarListaDistritoProveedorByText(rpta, value) {
    if (rpta != "") {
        lista = rpta.split("*");
        listarDistritoProveedorByText(lista, value);
    }
}
function listarDistritoProveedorByText(lista, value) {
    var dist = [];
    var nRegistros = lista.length;
    var campos;
    var idProv = document.getElementById("selectProv").value;
    var idDpto = document.getElementById("selectDept").value;
    for (var i = 0; i < nRegistros; i++) {
        campos = lista[i].split("|");
        if (idProv == campos[2] && campos[1] == idDpto && campos[3] != "00") {
            dist.push(campos[3] + "|" + campos[4]);
        }
    }
    crearCombo(dist, "selectDist", "---")
    $("#selectDist").find('option:contains("' + value + '")').prop('selected', true);
}